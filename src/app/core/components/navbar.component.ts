import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { getTheme } from '../store/settings/settings.selector';
import { AppState } from '../../app.module';
import { updateTheme } from '../store/settings/settings.actions';
import { logout } from '../store/auth/auth.actions';

@Component({
  selector: 'app-navbar',
  template: `
    {{theme$ | async}}
    <div [ngClass]="{
      'dark': (theme$ | async) === 'dark',
      'light': (theme$ | async) === 'light'
    }">
      <button routerLink="/custom" *appIfLogged>Customers</button>
      <button routerLink="/users">users</button>
      <button routerLink="/login">login</button>
      <button routerLink="/admin">admin</button>
      <button (click)="logoutHandler()">LOGOUT</button>
    </div>
    
    <button (click)="setTheme('light')">L</button>
    <button (click)="setTheme('dark')">D</button>
    <hr>
    
  `,
  styles: [
  ]
})
export class NavbarComponent {
  theme$: Observable<string> = this.store.pipe(select(getTheme))

  constructor(private store: Store<AppState>) { }

  setTheme(value: string) {
    this.store.dispatch(updateTheme({ value }));
  }

  logoutHandler() {
    this.store.dispatch(logout())
  }
}
