import { ActionReducerMap } from '@ngrx/store';
import { AppState } from '../../app.module';
import { customersReducers } from '../../features/customers/store/reducers';
import { settingsReducer } from './settings/settings.reducer';
import { routerReducer } from '@ngrx/router-store';
import { authReducer } from './auth/auth.reducer';

export const reducers: ActionReducerMap<AppState> = {
  // auth: () => ({ token: 123}),
  settings: settingsReducer,
  router: routerReducer,
  auth: authReducer
}
