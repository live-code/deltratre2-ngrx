// app/core/store/selectors/auth.selectors.ts
import { createSelector } from '@ngrx/store';
import { AuthState } from './auth.reducer';
import { AppState } from '../../../app.module';

export const getAuth = (state: AppState) =>  state.auth;

export const getToken = createSelector(
  getAuth,
  (state: AuthState) => state.accessToken
);

export const getLoggedStatus = createSelector(
  getAuth,
  (state: AuthState) => !!state.accessToken
);
