// app/core/auth/auth.reducer.ts
import { Action, createReducer, on } from '@ngrx/store';
import * as AuthActions from './auth.actions';

export interface AuthState {
  accessToken?: string;
  error?: boolean;
}

export const initialState: AuthState = {};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.saveFromLocalStorageInStore, (state, action) => ({...action.auth, error: false})),
  on(AuthActions.loginSuccess, (state, action) => ({...action.auth, error: false})),
  on(AuthActions.loginFailed, state => ({...state, error: true})),
  on(AuthActions.logoutSuccess, () => ({accessToken: null, error: false})),
);
