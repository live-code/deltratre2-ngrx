// app/core/auth/iflogged.directive.ts
import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { AppState } from '../../../app.module';
import { getLoggedStatus, getToken } from './auth.selector';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective implements OnInit, OnDestroy {

  private destroy$ = new Subject();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private store: Store<AppState>,
  ) {
  }

  ngOnInit() {
    this.store
      .pipe(
        select(getLoggedStatus),
        distinctUntilChanged(),
        takeUntil(this.destroy$)
      )
      .subscribe(isLogged => {
        this.view.clear();
        if (isLogged) {
          this.view.createEmbeddedView(this.template);
        }
      });
}

  ngOnDestroy() {
    this.destroy$.next();
  }
}
