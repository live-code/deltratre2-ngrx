// app/core/auth/auth.effects
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, filter, map, mapTo, switchMapTo, tap } from 'rxjs/operators';
import * as AuthActions from './auth.actions';
import { Auth } from './auth';
import { AuthService } from './auth.service';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthEffects {

  initEffect$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mapTo(this.authService.getToken()),
    filter(accessToken => !!accessToken),
    map(accessToken => AuthActions.saveFromLocalStorageInStore({ auth: { accessToken }}))
  ));

  // when login is invoked: invoke `login` endpoint and dispatch the success (or fail) action
  loginEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      exhaustMap(action =>
        this.authService.login(action.email, action.password).pipe(
          map((auth: Auth) => AuthActions.loginSuccess({auth})),
          catchError(() => of(AuthActions.loginFailed())),
        )
      )
    )
  );

  // after login success, save token to localstorage and
  // redirect to `users` page
  loginSuccessEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      tap(action => this.authService.saveAuth(action.auth)),
      mapTo(go({path: 'users'})),
    ),
  );

  // after logout, redirect to login and dispatch the logoutSuccess
  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(action => this.authService.cleanAuth()),
      switchMapTo([
        go({path: 'login'}),
        AuthActions.logoutSuccess()
      ]),
    )
  );

  // `logoutSuccess` has no side effect so it's not handled here.
  // anyway it is used in auth.reducer to clean the store

  constructor(
    private actions$: Actions,
    private authService: AuthService,
  ) {
  }
}
