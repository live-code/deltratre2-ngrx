// app/core/auth/auth.guard.ts
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map, tap } from 'rxjs/operators';
import { go } from '../router/router.actions';
import { AppState } from '../../../app.module';
import { getToken, getLoggedStatus } from './auth.selector';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private store: Store<AppState>) {}

  canActivate() {
    return this.store.pipe(
      select(getLoggedStatus),
      tap(isLogged => {
        if (!isLogged) {
          this.store.dispatch(go({ path: '/login' }));
        }
      })
    );
  }
}
