import { createAction, props } from '@ngrx/store';
import { SettingsState } from './settings.reducer';

export const loadSettings = createAction(
  '[Settings] Load Settings'
);

export const loadSettingsSuccess = createAction(
  '[Settings] Load Settings Success',
  props<{ settings: SettingsState }>()
);

export const loadSettingsFail = createAction(
  '[Settings] Load Settings Fail',
);

export const updateTheme = createAction(
  '[Settings] Update Theme',
  props<{ value: string }>()
);

export const updateThemeSuccess = createAction(
  '[Settings] Update Theme Success',
  props<{ value: string }>()
);

export const updateThemeFail = createAction(
  '[Settings] Update Theme Fail',
);
