import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { loadSettings, loadSettingsFail, loadSettingsSuccess, updateTheme, updateThemeFail, updateThemeSuccess } from './settings.actions';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { SettingsState } from './settings.reducer';

@Injectable({ providedIn: 'root'})
export class SettingsEffects {

  loadSettings$ = createEffect(() => this.actions$.pipe(
    ofType(loadSettings),
    mergeMap(
      () => this.http.get<SettingsState>('http://localhost:3000/settings')
        .pipe(
          map((data) => loadSettingsSuccess({ settings: data})),
          catchError(() => of(loadSettingsFail()))
        )
    )
  ))

  updateTheme$ = createEffect(() => this.actions$.pipe(
    ofType(updateTheme),
    mergeMap(
      (action) => this.http.patch('http://localhost:3000/settings', { theme: action.value})
        .pipe(
          map(() => updateThemeSuccess({ value: action.value})),
          catchError(() => of(updateThemeFail())),
        )
    )
  ))

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
