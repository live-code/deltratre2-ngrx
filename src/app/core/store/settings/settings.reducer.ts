import { createReducer, on } from '@ngrx/store';
import * as SettingsActions from './settings.actions';

export interface SettingsState {
  theme?: string;
  fontSize?: string;
  color?: string;
}

const INITIAL_STATE: SettingsState = { theme: 'light' };

export const settingsReducer = createReducer(
  INITIAL_STATE,
  on(SettingsActions.loadSettingsSuccess, (state, action) => ({...action.settings})),
  on(SettingsActions.loadSettingsSuccess, (state, action) => ({...action.settings})),
  on(SettingsActions.updateThemeSuccess, (state, action) => ({ ...state, theme: action.value}) ),
);
