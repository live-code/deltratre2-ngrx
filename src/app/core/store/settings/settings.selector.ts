import { AppState } from '../../../app.module';
import { createSelector } from '@ngrx/store';
import { SettingsState } from './settings.reducer';

export const getSettings = (state: AppState) => state.settings;

export const getTheme = createSelector(
  getSettings,
  (state: SettingsState) => state.theme
);


/*
export const getTheme = (state: AppState) => state.settings.theme;

export const getFont = (state: AppState) => state.settings.fontSize;
*/
