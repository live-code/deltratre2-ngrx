import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileModule } from './features/profile/profile.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './features/customers/store/effects/items.effects';
import { extModules } from './build-specifics';
import { reducers } from './core/store';
import { SettingsState } from './core/store/settings/settings.reducer';
import { NavbarComponent } from './core/components/navbar.component';
import { SettingsEffects } from './core/store/settings/settings.effects';
import { RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/store/router/router.effects';
import { AuthState } from './core/store/auth/auth.reducer';
import { AuthEffects } from './core/store/auth/auth.effects';
import { AuthInterceptor } from './core/store/auth/auth.interceptor';
import { IfLoggedDirective } from './core/store/auth/iflogged.directive';

export interface AppState {
  auth: AuthState;
  settings: SettingsState;
  router: RouterReducerState;
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IfLoggedDirective
  ],
  imports: [
    BrowserModule,
    ProfileModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot(
      reducers,
      {
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true,
          strictStateSerializability: true,
          strictActionSerializability: true,
        },
      }
    ),
    extModules,
    EffectsModule.forRoot([
      ItemsEffects,
      SettingsEffects,
      RouterEffects,
      AuthEffects
    ]),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

