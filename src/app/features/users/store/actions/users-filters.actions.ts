import { createAction, props } from '@ngrx/store';

export const filterByName = createAction(
  '[User] filter by name',
  props<{value: string}>()
);
