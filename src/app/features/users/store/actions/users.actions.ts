// users/store/actions/users.actions.ts
import { createAction, props } from '@ngrx/store';
import { User } from '../../model/user';

export const setActiveUser = createAction(
  '[User] Set active',
  props<{user: User}>()
);
export const cleanActiveUser = createAction(
  '[User] Reset'
);

export const loadUsers = createAction(
  '[Users] Load'
);
export const loadUsersSuccess = createAction(
  '[Users] Load Success',
  props<{ users: User[]}>()
);
export const loadUsersFailed = createAction(
  '[Users] Load Failed'
);

export const saveUser = createAction(
  '[Users] Save',
  props<{ user: User}>()
);

export const addUser = createAction(
  '[Users] Add',
  props<{ user: User}>()
);
export const addUserSuccess = createAction(
  '[Users] Add Success',
  props<{ user: User}>()
);
export const addUserFailed = createAction(
  '[Users] Add Failed'
);

export const editUser = createAction(
  '[Users] Edit',
  props<{ user: User}>()
);
export const editUserSuccess = createAction(
  '[Users] Edit Success',
  props<{ user: User}>()
);
export const editUserFailed = createAction(
  '[Users] Edit Failed'
);

export const deleteUser = createAction(
  '[Users] Delete',
  props<{ id: number}>()
);
export const deleteUserSuccess = createAction(
  '[Users] Delete Success',
  props<{ id: number}>()
);
export const deleteUserFailed = createAction(
  '[Users] Delete Failed'
);
