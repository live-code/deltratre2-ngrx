import { User } from '../../model/user';
import { createReducer, on } from '@ngrx/store';
import * as UsersActions from '../actions/users.actions';

export const initialState: User[] = [];

export const usersReducer = createReducer(
  initialState,
  on(UsersActions.loadUsersSuccess, (state, action) => [...action.users]),
  on(UsersActions.deleteUserSuccess, (state, action) => state.filter(user => user.id !== action.id)),
  on(UsersActions.addUserSuccess, (state, action) => [...state, action.user]),
  on(UsersActions.editUserSuccess, (state, action) => state.map(item => {
    return item.id  === action.user.id ? action.user : item;
  }))
);
