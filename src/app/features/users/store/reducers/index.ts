import { User } from '../../model/user';
import { ActionReducerMap } from '@ngrx/store';
import { usersReducer } from './users.reducer';
import { userReducer } from './user.reducer';
import { filterReducer, UsersFiltersState } from './users-filters.reducer';

export interface UsersState {
  list: User[];
  active: User;
  filters: UsersFiltersState;
}

export const usersReducers: ActionReducerMap<UsersState> = {
  list: usersReducer,
  active: userReducer,
  filters: filterReducer
}
