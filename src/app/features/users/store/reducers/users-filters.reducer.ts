// features/users/store/reducers/users-filters.reducer.ts
import { createReducer, on } from '@ngrx/store';
import { filterByName } from '../actions/users-filters.actions';
// import * as UsersFilterActions from '../actions/users-filters.actions';

export const initialState = {
  name: ''
};

export interface UsersFiltersState {
  name: string;
}

export const filterReducer = createReducer(
  initialState,
  on(filterByName, (state, action) => ({ ...state, name: action.value}))
);
