import { createReducer, on } from '@ngrx/store';
import { User } from '../../model/user';
import { addUserSuccess, cleanActiveUser, deleteUserSuccess, setActiveUser } from '../actions/users.actions';

export const INITIAL_STATE: User = {}

export const userReducer = createReducer(
  INITIAL_STATE,
  on(setActiveUser, (state, action) =>  {
    return ({...action.user });
  } ),
  on(cleanActiveUser, () => ({...INITIAL_STATE})),
  on(deleteUserSuccess, (state: User, action) => {
    return state.id === action.id ? {} : state ;
  }),
  // on(addUserSuccess, (state, action) => ({ ...action.user})),
);
