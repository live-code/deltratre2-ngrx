import { AppState } from '../../../../app.module';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from '../../model/user';
import { UsersState } from '../reducers';
import { UsersFiltersState } from '../reducers/users-filters.reducer';


export const getUsersFeature = createFeatureSelector<UsersState>('users');

export const getFilters = createSelector(
  getUsersFeature,
  (state: UsersState) => state.filters
);

export const getUsersList = createSelector(
  getUsersFeature,
  (state: UsersState) => {
    return state.list;
  }
)

export const getFilteredUsersList = createSelector(
  getUsersList,
  getFilters,
  (users: User[], filters: UsersFiltersState) => {
    console.log('qui')
    return users.filter(u => {
      return u.name.toLowerCase().indexOf(filters.name.toLowerCase()) !== -1;
    })
  }
)

export const getActiveUser = createSelector(
  getUsersFeature,
  (state: UsersState) => state.active
)
