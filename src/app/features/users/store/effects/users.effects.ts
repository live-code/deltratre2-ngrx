// features/users/store/effects/users.effects
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { mergeMap, catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';

import { UsersService } from '../../services/users.service';
import * as UsersActions from '../actions/users.actions';
import { saveUser } from '../actions/users.actions';
import { select, Store } from '@ngrx/store';
import { getActiveUser } from '../selectors/users.selectors';


@Injectable()
export class UsersEffects {

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.loadUsers),
    mergeMap(() =>
      this.userService.loadUsers()
        .pipe(
          map(data => UsersActions.loadUsersSuccess({ users: data})),
          catchError(() => of(UsersActions.loadUsersFailed())
        )
      )
    )
  ));

  deleteUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.deleteUser),
    mergeMap(action =>
      this.userService.deleteUser(action.id)
        .pipe(
          map(() => UsersActions.deleteUserSuccess({ id: action.id})),
          catchError(() => of(UsersActions.deleteUserFailed()))
        )
    )
  ));

  saveUsers$ = createEffect(() => this.actions$.pipe(
    ofType(saveUser),
    withLatestFrom(this.store.pipe(select(getActiveUser))),
    mergeMap(([action, active]) => {
      if (active.id) {
        return of(UsersActions.editUser({ user: {...action.user}}));
      } else {
        return of(UsersActions.addUser({ user: action.user}));
      }
    })
  ))


  addUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.addUser),
    mergeMap(action =>
      this.userService.addUser(action.user)
        .pipe(
          map(data => UsersActions.addUserSuccess({ user: data})),
          catchError(() => of(UsersActions.addUserFailed())
          )
        )
    )
  ));

  editUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.editUser),
    mergeMap(action =>
      this.userService.editUser(action.user)
        .pipe(
          // If successful, dispatch success action with result
          map(data => UsersActions.editUserSuccess({ user: data})),
          // If request fails, dispatch failed action
          catchError(() => of(UsersActions.editUserFailed())
          )
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private userService: UsersService,
    private store: Store
  ) {}
}
