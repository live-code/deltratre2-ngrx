import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/effects/users.effects';
import { UsersService } from './services/users.service';
import { FormsModule } from '@angular/forms';
import { usersReducers } from './store/reducers';


@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    StoreModule.forFeature('users', usersReducers),
    EffectsModule.forFeature([UsersEffects])
  ],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
