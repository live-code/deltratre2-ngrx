import { Component, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { User } from './model/user';
import { addUserSuccess, cleanActiveUser, deleteUser, loadUsers, saveUser, setActiveUser } from './store/actions/users.actions';
import { Observable } from 'rxjs';
import { getActiveUser, getFilteredUsersList, getFilters, getUsersList } from './store/selectors/users.selectors';
import { NgForm } from '@angular/forms';
import { UsersState } from './store/reducers';
import { filterByName } from './store/actions/users-filters.actions';
import { UsersFiltersState } from './store/reducers/users-filters.reducer';
import { go } from '../../core/store/router/router.actions';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'app-users',
  template: `
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input 
        type="text"
        [ngModel]="(active$ | async).name"
        name="name"
        placeholder="Add your name"
        required
      >
      <button type="submit">SAVE</button>
      <button type="button" (click)="resetHandler(f)">RESET</button>
    </form>

    <input
      type="text"
      placeholder="Filter by name"
      [ngModel]="(filters$ | async).name"
      (input)="filterByNameHandler(inputFilterName.value)" 
      #inputFilterName
    >

    <li 
      *ngFor="let user of (users$ | async)"
      (click)="setActiveHandler(user)"
      [style.color]="user.id === (active$ | async).id ? 'red' : null"
    >
      {{user.name}}
      <button (click)="deleteHandler(user, $event)">Delete</button>
    </li>
   
    <pre>{{ active$ | async | json}}</pre>
  `,
  styles: [
  ]
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]> = this.store.pipe(select(getFilteredUsersList));
  active$: Observable<User> = this.store.pipe(select(getActiveUser))
  filters$: Observable<UsersFiltersState> = this.store.pipe(select(getFilters));
  @ViewChild('f') form: NgForm;

  active: User;
  constructor(
    private store: Store<UsersState>,
    private actions$: Actions
  ) { }

  ngOnInit(): void {
    this.store.dispatch(loadUsers());
    this.active$.subscribe(v => this.active = v);

    this.actions$
      .pipe(
        ofType(addUserSuccess, cleanActiveUser)
      )
      .subscribe(() => {
        this.form.reset();
      });
  }

  deleteHandler(user: User, event: MouseEvent) {
    event.stopPropagation();
    this.store.dispatch(deleteUser({ id: user.id}));
  }

  saveHandler(f: NgForm) {
    this.store.dispatch(saveUser({ user: f.value }));
  }

  setActiveHandler(user: User) {
    // this.store.dispatch(go({ path: 'custom'}));
    this.store.dispatch(setActiveUser({ user }));
  }

  resetHandler(f: NgForm) {
    this.store.dispatch(cleanActiveUser());
  }

  filterByNameHandler(text: string) {
    this.store.dispatch(filterByName({ value: text }));
  }

}
