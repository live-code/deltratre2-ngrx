import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addItem, addItemFail,
  addItemSuccess,
  deleteItem,
  deleteItemFail,
  deleteItemSuccess,
  loadItems,
  loadItemsFail,
  loadItemsSuccess
} from '../actions/items.actions';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Item } from '../../model/item';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../app.module';

@Injectable({ providedIn: 'root' })
export class ItemsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    mergeMap(
      () => this.http.get<Item[]>('http://localhost:3000/users')
        .pipe(
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFail()))
        )
    )
  ))

  deleteItems$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(
      ({id}) => this.http.delete('http://localhost:3000/users/' + id)
        .pipe(
          map(() => deleteItemSuccess({ id })),
          catchError(() => of(deleteItemFail()))
        )
    )
  ));

  addItems$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    mergeMap(
      (action) => this.http.post<Item>('http://localhost:3000/users/', action.item)
        .pipe(
          map((item) => addItemSuccess({ item })),
          catchError(() => of(addItemFail()))
        )
    )
  ))

  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private http: HttpClient
  ) {
  }
}
