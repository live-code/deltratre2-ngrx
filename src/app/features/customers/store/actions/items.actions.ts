import { createAction, props } from '@ngrx/store';
import { Item } from '../../model/item';

export const loadItems = createAction(
  '[Items] load',
);

export const loadItemsSuccess = createAction(
  '[Items] load success',
  props<{ items: Item[]}>()
);

export const loadItemsFail = createAction(
  '[Items] load fail',
);


export const addItem = createAction(
  '[Item] Add',
  props<{ item: Item }>()
);

export const addItemSuccess = createAction(
  '[Item] Add Success',
  props<{ item: Item }>()
);

export const addItemFail = createAction(
  '[Item] Add Item Fail',
);



export const deleteItem = createAction(
  '[Item] Delete',
  props<{ id: number }>()
);

export const deleteItemSuccess = createAction(
  '[Item] Delete Success',
  props<{ id: number }>()
);

export const deleteItemFail = createAction(
  '[Item] Delete Item Fail',
);
