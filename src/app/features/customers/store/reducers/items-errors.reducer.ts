import { createReducer } from '@ngrx/store';

export const itemErrorReducer = createReducer(
  { error: false }
);
