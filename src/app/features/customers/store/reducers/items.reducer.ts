import { createReducer, on } from '@ngrx/store';
import { addItem, addItemFail, addItemSuccess, deleteItem, deleteItemSuccess, loadItemsSuccess } from '../actions/items.actions';
import { Item } from '../../model/item';


const INITIAL_STATE: Item[] = [];

export const itemReducer = createReducer(
  INITIAL_STATE,
  on(loadItemsSuccess, (state, action) => [...action.items]),
  on(deleteItemSuccess, (state, action) => state.filter(item => item.id !== action.id)),
  on(addItemSuccess, (state, action) =>  [...state, action.item])
);



