import { ActionReducerMap } from '@ngrx/store';
import { itemReducer } from './items.reducer';
import { itemErrorReducer } from './items-errors.reducer';
import { Item } from '../../model/item';

export interface CustomersState {
  items: Item[];
  errors: any;
}


export const customersReducers: ActionReducerMap<CustomersState> = {
  items: itemReducer,
  errors: itemErrorReducer
};
