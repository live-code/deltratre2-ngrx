import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { customersReducers } from './store/reducers';


@NgModule({
  declarations: [CustomersComponent],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    FormsModule,
    StoreModule.forFeature('customers', customersReducers)
  ]
})
export class CustomersModule { }
