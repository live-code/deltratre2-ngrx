import { Component, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as ItemActions from './store/actions/items.actions';
import { Item } from './model/item';
import { getItems } from './store/selectors/items.selector';
import { NgForm } from '@angular/forms';
import { filter } from 'rxjs/operators';
import { Actions, ofType } from '@ngrx/effects';
import { addItemSuccess } from './store/actions/items.actions';
import { AppState } from '../../app.module';

@Component({
  selector: 'app-customer',
  template: `
    <!--    
    <button routerLink="/custom">Customers</button>
    <button routerLink="/users">users</button>
    <button routerLink="/login">login</button>
    <button routerLink="/admin">admin</button>
    -->

    <router-outlet></router-outlet>

    <form #f="ngForm" (submit)="saveHandler(f)">
      <input type="text" [ngModel] name="name" placeholder="Name">
      <button type="submit">SAVE</button>
    </form>

    <h3>Items</h3>

    <li *ngFor="let item of (items$ | async)">
      {{item.name}}
      <button (click)="deleteHandler(item)">Del</button>
    </li>

    {{ items$ | async | json}}
  `,
  styles: []
})
export class CustomersComponent {
  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.pipe(select(getItems));
  // isError$: Observable<boolean> = this.store.pipe(select(state => !state.items.isSuccess));

  constructor(private store: Store<AppState>, actions$: Actions) {
    this.store.dispatch(ItemActions.loadItems());

    actions$
      .pipe(
        ofType(addItemSuccess),
      )
      .subscribe(() => this.form.reset())
  }

  deleteHandler(item: Item) {
    this.store.dispatch(ItemActions.deleteItem({ id: item.id}))
  }

  saveHandler(f: NgForm) {
    const item = { id: Date.now(), ...f.value };
    this.store.dispatch(ItemActions.addItem({ item }));

    // f.reset();
  }
}
