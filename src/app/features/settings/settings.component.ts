import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { loadSettings, updateTheme } from '../../core/store/settings/settings.actions';
import { Observable, Subscription } from 'rxjs';
import { SettingsState } from '../../core/store/settings/settings.reducer';
import { getSettings, getTheme } from '../../core/store/settings/settings.selector';

@Component({
  selector: 'app-settings',
  template: `
    <div *ngIf="(theme$ | async )  as theme ">
      <button
          [style.background]="theme === 'light' ? 'red' : null"
          (click)="setTheme('light')">light</button>
      <button
        [style.background]="theme === 'dark' ? 'red' : null"
        (click)="setTheme('dark')">dark</button>
    </div>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {
  settings$: Observable<SettingsState> = this.store.pipe(select(getSettings))
  theme$: Observable<string> = this.store.pipe(select(getTheme))

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(loadSettings());
  }

  setTheme(value: string) {
    this.store.dispatch(updateTheme({ value }));
  }

}
