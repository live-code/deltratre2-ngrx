// app/features/login/containers/login-page.component.ts
import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getAuth } from '../../core/store/auth/auth.selector';
import { AppState } from '../../app.module';
import { login } from '../../core/store/auth/auth.actions';
import { Credentials } from '../../core/store/auth/auth';


@Component({
  selector: 'app-login-page',
  template: `
    <form #f="ngForm" (submit)="signin(f.value)" class="container mt-5" style="max-width: 400px;">
      <h2>Login</h2>

      <div *ngIf="(auth$ | async)?.error as error">
        Wrong credentials
      </div>

      <div class="form-group">
        <input type="text"
               placeholder="email"
               [ngModel] name="email"  required>
      </div>
      <div class="form-group">
        <input 
          type="password"
          placeholder="password" [ngModel] name="password" required>
      </div>

      <button type="submit">
        SIGN IN
      </button>
    </form>

  `,
  styles: []
})
export class LoginComponent {
  auth$ = this.store.pipe(select(getAuth));

  constructor(private store: Store<AppState>) {}

  signin(formData: Credentials) {
    const { email, password } = formData;
    this.store.dispatch(login({email, password}));
  }
}
